#!/usr/bin/env python3

import threading
import time
import uuid
from sys import argv

import actions
import paho.mqtt.client as mqtt
from pytradfri import Gateway
from pytradfri.api.libcoap_api import APIFactory
from pytradfri.error import PytradfriError
from pytradfri.util import load_json, save_json

CONFIG_FILE = "tradfri_standalone_psk.conf"
devices = []


def handleMqttMessage(topic: str, msg, client):
    global devices
    global api
    old_topic = topic.split("/")
    topic = topic.split("/")[1:]
    if len(topic) != 2:
        return
    device = None
    for d in devices:
        if str(d.id) == topic[0]:
            device = d
            break
    if device is None:
        return
    if topic[1] in actions.actions:
        command = actions.actions[topic[1]](device, msg)
        try:
            api(command)
            echo_topic = old_topic[:]
            echo_topic.insert(-1, "echo")
            print('/'.join(echo_topic))
            client.publish('/'.join(echo_topic), msg)
        except:
            time.sleep(0.1)
            print("error")
            handleMqttMessage('/'.join(old_topic), msg, client)
    print(device, topic[1], msg)


def onConnect(client, userdata, flags, rc):
    print("Connesso al broker")
    client.subscribe("tradfri/#")
    try:
        client.subscribe(argv[3])
    except:
        pass


def onMessage(client, userdata, msg):
    #print("Messaggio ricevuto")
    try:
        if msg.topic == argv[3]:
            client.publish(msg.payload)
            return
    except:
        pass
    handleMqttMessage(msg.topic, msg.payload.decode('ascii'), client)


def observe(api, device):
    """Observe."""
    TASK_DURATION = 3600

    def callback(updated_device):
        global client
        try:
            client
        except:
            print("Client doesn't exist yet")
            return
        print("Received message for: %s" % updated_device.name)
        if updated_device.has_light_control:
            light = updated_device.light_control.lights[0]
            assert "power" in actions.actions
            client.publish("tradfri/" + str(updated_device.id) +
                           "/status/power", "true" if light.state else "false")
            #
            assert "brightness" in actions.actions
            client.publish("tradfri/" + str(updated_device.id) +
                           "/status/brightness", str(light.dimmer*100/254))
            #
            assert "colour_temperature" in actions.actions
            client.publish("tradfri/" + str(updated_device.id) +
                           "/status/colour_temperature", str(light.color_temp))
        elif updated_device.has_socket_control:
            assert "power" in actions.actions
            client.publish("tradfri/" + str(updated_device.id) + "/status/power",
                           "true" if updated_device.socket_control.sockets[0].state else "false")

    def err_callback(err):
        print(err)

    def worker():
        while True:
            api(device.observe(callback, err_callback, duration=TASK_DURATION))

    threading.Thread(target=worker, daemon=True).start()
    print("Sleeping to start observation task")
    time.sleep(1)


"""Run."""
# Assign configuration variables.
# The configuration check takes care they are present.
conf = load_json(CONFIG_FILE)

try:
    identity = conf[argv[1]].get("identity")
    psk = conf[argv[1]].get("key")
    api_factory = APIFactory(host=argv[1], psk_id=identity, psk=psk)
except KeyError:
    identity = uuid.uuid4().hex
    api_factory = APIFactory(host=argv[1], psk_id=identity)

    try:
        psk = api_factory.generate_psk(argv[2])
        print("Generated PSK: ", psk)

        conf[argv[1]] = {"identity": identity, "key": psk}
        save_json(CONFIG_FILE, conf)
    except AttributeError:
        raise PytradfriError(
            "Please provide the 'Security Code' on the "
            "back of your Tradfri gateway using the "
            "-K flag."
        )

api = api_factory.request

gateway = Gateway()

devices_command = gateway.get_devices()
devices_commands = api(devices_command)
devices = [d for d in api(devices_commands)
           if d.device_info.manufacturer != "IKEA of Sweden"]
for d in devices:
    observe(api, d)

client = mqtt.Client()
client.on_connect = onConnect
client.on_message = onMessage

client.connect(argv[2], 1883, 60)

client.loop_forever()
