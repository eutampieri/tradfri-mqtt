FROM registry.gitlab.com/eutampieri/mqtt-healthcheck AS builder
FROM python:3-alpine
HEALTHCHECK CMD /mqtt_health
COPY --from=builder /tools/mqtt_health /
RUN apk add mosquitto-clients
RUN apk add libcoap
RUN apk add jq
RUN pip3 install paho-mqtt
RUN pip3 install pytradfri
COPY *.py .
WORKDIR /config
ENTRYPOINT python3 /main.py `jq .gateway_addr <config.json` `jq .broker_addr <config.json`
