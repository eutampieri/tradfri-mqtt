def set_power(dev, status):
    on = status == "true"
    if dev.has_light_control:
        return dev.light_control.set_state(on)
    elif dev.has_socket_control:
        return dev.socket_control.set_state(on)

def set_brightness(dev, value):
    brightness = int(int(value) * 254 / 100)
    if dev.has_light_control:
        return dev.light_control.set_dimmer(brightness, transition_time=100)

def set_colour_temperature(dev, value):
    mireds = int(value)
    if dev.has_light_control:
        return dev.light_control.set_color_temp(mireds)

actions = {
    "power": set_power,
    "colour_temperature": set_colour_temperature,
    "brightness": set_brightness
}